from piston.handler import BaseHandler, AnonymousBaseHandler
from piston.utils import rc, validate
from sumatra.recordstore.django_store import models
from scores.models import RecordScore
import logging

logger = logging.getLogger("scores_app")


def check_permissions(func):
    def wrapper(self, request, project, *args, **kwargs):
        if models.Project.objects.filter(
                id=project,
                projectpermission__user__username='anonymous').count():
            return func(self, request, project, *args, **kwargs)
        elif request.user.projectpermission_set.filter(project__id=project).count():
            return func(self, request, project, *args, **kwargs)
        else:
            return rc.FORBIDDEN
    return wrapper


class AnonymousScoreListHandler(AnonymousBaseHandler):
    allowed_methods = ('GET',)
    model = RecordScore
    # fields = ('variable', 'index', 'rel_variable', 'value', 'record')
    fields = ('variable', 'index', 'rel_variable', 'value')
    template = "score_list.html"

    def read(self, request, project, label):
        try:
            record = models.Record.objects.get(project=project, label=label)
        except:
            return rc.NOT_FOUND
        try:
            prj = models.Project.objects.get(
                id=project,
                projectpermission__user__username='anonymous')
        except models.Project.DoesNotExist:
            resp = HttpResponse(
                "Authorization Required",
                content_type='text/plain', status=401)
            resp['WWW-Authenticate'] = 'Basic realm="%s"' % "Sumatra Server API"
            return resp
        filter = {'record': record}
        try:
            return self.queryset(request).filter(**filter)
        except:
            return rc.NOT_FOUND


class ScoreListHandler(BaseHandler):
    allowed_methods = ('GET', 'POST',)
    model = RecordScore
    # fields = ('variable', 'index', 'rel_variable', 'value', 'record')
    fields = ('variable', 'index', 'rel_variable', 'value')
    template = "score_list.html"
    anonymous = AnonymousScoreListHandler

    @check_permissions
    def read(self, request, project, label):
        record = models.Record.objects.get(project=project, label=label)
        filter = {'record': record}
        try:
            return self.queryset(request).filter(**filter)
        except:
            return rc.NOT_FOUND

    @check_permissions
    def create(self, request, project, label):
        record = models.Record.objects.get(project=project, label=label)
        attrs = self.flatten_dict(request.data)

        # if self.exists(**attrs):
        #     return rc.DUPLICATE_ENTRY

        # else:

        score = RecordScore(
            record=record,
            variable=attrs['variable'],
            index=attrs['index'],
            rel_variable=attrs.get('rel_variable', None),
            value=attrs['value'])
        score.save()
        return rc.CREATED

    # @check_permissions
    # def delete(self, request, project, label, id):
    #     pass
