// Score
/*function show_score(data) {
    info = {scores: data}
    var template = '{{#scores}}<tr class="odd"><td>{{variable}}</td><td>{{index}}</td><td>{{rel_variable}}</td><td>{{value}}</td></tr>{{/scores}}';
    var html = Mustache.to_html(template, info);
    $('table.main').append(html);
}

$(document).ready(function() {
    $.getJSON(window.location.href, show_score);
  });*/


(function(angular){

	var app = angular.module('scoresApp', []);
	
	app.factory('showScores', ['$log', '$http', function($log, $http){
		var scores = {};
		scores.data = [];
		scores.uniqueVariables = [];
		scores.precision = [];
		scores.recall = [];
		scores.f1 = [];
		scores.accuracy = [];
		scores.total = 0.0;
		scores.classRatio = [];
		$http.get(window.location.href).success(function(data){
			scores.data = data;
			$.each(scores.data, function(index, value){
				if(value.index == 'confusion')
					scores.uniqueVariables.push(value.variable);
			});
			scores.uniqueVariables = scores.uniqueVariables.filter(function(item, i, ar){
				return ar.indexOf(item) === i;
			});
			
			
		});
		
		return scores;
	}]);
	
	app.controller('scoresController', ['$log', '$scope', 'showScores',
	                                  function($log, $scope, showScores){
		$scope.scoresData = showScores;
		$scope.displayPrecisionRecall = false;
		$scope.displayF1Accuracy = false;
		
		var total_real = [];
		var total_predicted = [];
		var true_positive = [];
		
		$scope.showPrecisionRecall = function(){
			
			$scope.displayPrecisionRecall = true;
	
			if($scope.scoresData.precision.length == 0){
				$.each($scope.scoresData.uniqueVariables, function(index, value){
					total_real.push(0.0);
					total_predicted.push(0.0);
					true_positive.push(0.0);
					$scope.scoresData.precision.push(0.0);
					$scope.scoresData.recall.push(0.0);
					$scope.scoresData.f1.push(0.0);
					$scope.scoresData.accuracy.push(0.0);
					$scope.scoresData.classRatio.push(0.0);
				});
				$.each($scope.scoresData.data, function(index, value){
					if(value.index == 'confusion'){
						id = $scope.scoresData.uniqueVariables.indexOf(value.variable);
						total_real[id] += parseFloat(value.value);
						id_rel = $scope.scoresData.uniqueVariables.indexOf(value.rel_variable);
						total_predicted[id_rel] += parseFloat(value.value);
						if(value.variable == value.rel_variable)
							true_positive[id] = parseFloat(value.value);
					}
				});
				
				$.each($scope.scoresData.data, function(index, value){
					if(value.index == 'confusion'){
						if(value.variable == value.rel_variable){
							id =  $scope.scoresData.uniqueVariables.indexOf(value.variable)
							$scope.scoresData.recall[id] = value.value / total_real[id];
							$scope.scoresData.precision[id] = value.value / total_predicted[id];
						}
					}
				});
				$.each($scope.scoresData.precision, function(index, precision){
					$scope.scoresData.total += total_real[index];
				});
				$.each($scope.scoresData.precision, function(index, precision){
					$scope.scoresData.classRatio[index] += total_real[index] / $scope.scoresData.total;
				});
				
			}
		};
		
		$scope.showF1Accuracy = function(){
			$scope.displayF1Accuracy = true;
			if($scope.scoresData.precision.length == 0){
				var isPrecisionDisplayed = $scope.displayPrecisionRecall;
				$scope.showPrecisionRecall();
				$scope.displayPrecisionRecall = isPrecisionDisplayed;
			}
			$.each($scope.scoresData.precision, function(index, precision){
				recall = $scope.scoresData.recall[index]
				$scope.scoresData.f1[index] = 2.0 * (precision * recall / (precision + recall));
			});
			
			$.each($scope.scoresData.uniqueVariables, function(index, variable){
				var true_negative = $scope.scoresData.total - total_predicted[index] - total_real[index];
				// By doing the above, I removed twice true_positive;
				true_negative += true_positive[index];
				$scope.scoresData.accuracy[index] = (true_negative + true_positive[index]) / $scope.scoresData.total;
			});

		}
		
	}]);

})(window.angular);