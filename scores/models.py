from django.db import models
from sumatra.recordstore.django_store.models import Record


class RecordScore(models.Model):
    record = models.ForeignKey(Record)
    variable = models.CharField(max_length=20)
    index = models.CharField(max_length=50)
    rel_variable = models.CharField(max_length=20, blank=True, null=True)
    value = models.DecimalField(max_digits=15, decimal_places=6)

    def __unicode__(self):
        return u"Score %s-%s for %s" % (self.index, self.variable, self.record)
