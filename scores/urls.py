from django.conf.urls import patterns, url
from piston.authentication import HttpBasicAuthentication
from sumatra_server.authentication import DjangoAuthentication, AuthenticationDispatcher
# from sumatra_server.handlers import RecordHandler, ProjectHandler, ProjectListHandler, PermissionListHandler
from sumatra_server.resource import Resource
from scores.handlers import ScoreListHandler

auth = AuthenticationDispatcher({'html': DjangoAuthentication()},
                                default=HttpBasicAuthentication(realm='Sumatra Server API'))

scorelist_resource = Resource(ScoreListHandler, authentication=auth)

urlpatterns = patterns('',
    url(r'^(?P<project>[^/]+)/(?P<label>\w+[\w|\-\.]*)/scores/', scorelist_resource),
    # url(r'^$', scorelist_resource, name="score-list"),
)
