(function(){
angular.module('underscore',[]).
    factory('_', function(){
        return window._;
    });

var app = angular.module('sumatraServerApp', ['underscore']);

app.controller('recordListCtrl',['$http', '$log', function($http){
    var project = this;
    project.url = url;
    project.project = [];
    project.records = [];
    project.tags = [];
    $http.get(url)
        .success(function(data){
            for (var i in data.records){
                $http.get(data.records[i])
                    .success(function(data){
                        project.records.push(data);
                    });
            }
        });

    project.pushTag = function(event){
        project.tags.push(event.target.firstChild.textContent.trim());
        console.log(project.tags);
    };

    project.filterTag = function(item){
        if (project.tags.length === 0) {
            return true;
        } else {
            return _.every(
                project.tags,
                function (e){
                    return _.contains(item.tags, e);
                }
            );
        }
    };

    project.showAll = function(){
        project.tags = [];
        console.log(project.tags);
    };
    // for (record in this.project.records){
    //     console.log(record)
    // }
    // this.records = records;
}]);


var records = [
{
    "script_arguments": "",
    "platforms": [
        {
            "system_name": "Darwin",
            "ip_addr": "127.0.0.1",
            "architecture_bits": "64bit",
            "machine": "x86_64",
            "architecture_linkage": "",
            "version": "Darwin Kernel Version 13.3.0: Tue Jun  3 21:27:35 PDT 2014; root:xnu-2422.110.17~1/RELEASE_X86_64",
            "release": "13.3.0",
            "network_name": "hazard.local",
            "processor": "i386"
        }
    ],
    "duration": 0.00101304054260254,
    "diff": "",
    "datastore": {
        "type": "FileSystemDataStore",
        "parameters": {
            "root": "/Users/Juan/tests/python/Data"
        }
    },
    "input_datastore": {
        "type": "FileSystemDataStore",
        "parameters": {
            "root": "/"
        }
    },
    "executable": {
        "path": "//anaconda/envs/django-ecs/bin/python",
        "version": "2.7.6",
        "name": "Python",
        "options": ""
    },
    "parameters": {
        "content": "n = 100 # number of values to draw\nseed = 65785 # seed for random number generator\nsumatra_label = \"20140807-100957\"\ndistr = \"uniform\" # statistical distribution to draw values from",
        "type": "SimpleParameterSet"
    },
    "label": "20140807-100957",
    "version": "d415411b602c",
    "stdout_stderr": "Not launched.",
    "project_id": "TestProject",
    "repository": {
        "url": "/Users/Juan/tests/python",
        "type": "MercurialRepository",
        "upstream": null
    },
    "tags": [],
    "timestamp": "2014-08-07T10:09:57Z",
    "reason": "Test Api Score",
    "dependencies": [],
    "user": "Juan M. Barrios <juan.barrios@conabio.gob.mx>",
    "outcome": "",
    "main_file": "api_test.py",
    "input_data": [],
    "launch_mode": {
        "type": "SerialLaunchMode",
        "parameters": {
            "working_directory": "/Users/Juan/tests/python",
            "options": null
        }
    },
    "output_data": [],
    "repeats": null
},
{
    "script_arguments": "",
    "platforms": [
        {
            "system_name": "Darwin",
            "ip_addr": "127.0.0.1",
            "architecture_bits": "64bit",
            "machine": "x86_64",
            "architecture_linkage": "",
            "version": "Darwin Kernel Version 13.3.0: Tue Jun  3 21:27:35 PDT 2014; root:xnu-2422.110.17~1/RELEASE_X86_64",
            "release": "13.3.0",
            "network_name": "hazard.local",
            "processor": "i386"
        }
    ],
    "duration": 0.00100803375244141,
    "diff": "",
    "datastore": {
        "type": "FileSystemDataStore",
        "parameters": {
            "root": "/Users/Juan/tests/python/Data"
        }
    },
    "input_datastore": {
        "type": "FileSystemDataStore",
        "parameters": {
            "root": "/"
        }
    },
    "executable": {
        "path": "//anaconda/envs/django-ecs/bin/python",
        "version": "2.7.6",
        "name": "Python",
        "options": ""
    },
    "parameters": {
        "content": "n = 100 # number of values to draw\nseed = 65785 # seed for random number generator\nsumatra_label = \"20140807-100919\"\ndistr = \"uniform\" # statistical distribution to draw values from",
        "type": "SimpleParameterSet"
    },
    "label": "20140807-100919",
    "version": "d415411b602c",
    "stdout_stderr": "Not launched.",
    "project_id": "TestProject",
    "repository": {
        "url": "/Users/Juan/tests/python",
        "type": "MercurialRepository",
        "upstream": null
    },
    "tags": [],
    "timestamp": "2014-08-07T10:09:19Z",
    "reason": "Test Api Score",
    "dependencies": [],
    "user": "Juan M. Barrios <juan.barrios@conabio.gob.mx>",
    "outcome": "",
    "main_file": "api_test.py",
    "input_data": [],
    "launch_mode": {
        "type": "SerialLaunchMode",
        "parameters": {
            "working_directory": "/Users/Juan/tests/python",
            "options": null
        }
    },
    "output_data": [],
    "repeats": null
}
];

var url = window.location.href;
})();
