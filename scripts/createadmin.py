#!/usr/bin/env python

from django.contrib.auth.models import User

admin = User.objects.get(username__exact='admin')
admin.set_password('admin321')
admin.save()
