from django.conf.urls import patterns, include, url
from django.views.generic.base import TemplateView
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    (r'^records/', include('sumatra_server.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name':'login.html'}, name="login"),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout_then_login', {'login_url':'/'}, name="logout"),
    url(r'^admin/', include(admin.site.urls)),
    (r'^records/', include('scores.urls')),
)
