## Para desarrollar

Se necesita tener instalado MySQL y una base de datos con nombre sumatra. Todo esto se puede modificar en el archivo `ecoinfo_eb/local_settings.py`

Después correr los siguientes comandos:

```
python manage.py syncdb
python manage.py migrate
python manage.py loaddata data/fixtures.json
python manage.py runserver
```
